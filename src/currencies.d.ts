export declare type Currencies = Array<{
    name: string;
    symbol: string;
    validator: {
        isValidAddress: (address: string, currency: any, networkType: string, addressFormats: string[]) => boolean;
        verifyChecksum: (address: string) => boolean;
    };
    addressTypes?: {
        prod: string[];
        testnet?: string[];
    };
    iAddressTypes?: {
        prod: string[];
        testnet?: string[];
    };
    expectedLength?: number;
    hashFunction?: string;
    regex?: RegExp;
}>;
